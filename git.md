Git
===

Changing Git autocrlf
---------------------
```
# Disable windows autocrlf
git config --global core.eol lf
git config --global core.autocrlf input

# Reset eol git changes
git rm -rf --cached .
git reset --hard HEAD
```
